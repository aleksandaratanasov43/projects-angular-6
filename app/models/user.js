var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Thanks to http://blog.matoski.com/articles/jwt-express-node-mongoose/

// set up a mongoose model


var OrderSchema = new Schema({
    name: {
        type: String,
        requried: true
    },
    amount: {
        type: Number,
        requried: true
    }

})

var UserSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    surname: {
        type: String,
        required: true
    },
    orders: {
        type:OrderSchema,
        required:false
    },
    email: {
        type: String,
        unique: true,
        requried: true
    },
    city: {
        type: String,
        requried: true
    },
    state: {
        type: String,
        requried: true
    },
    address: {
        type: String,
        requried: true
    }
});


module.exports = mongoose.model('User', UserSchema)
module.exports = mongoose.model('Order', OrderSchema);