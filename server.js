
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var config = require('./config/database'); // get db config file
var port = process.env.PORT || 8080;
var ObjectID = require('mongodb').ObjectID;
var cors = require('cors');
app.use(cors({ origin: '*' }));

// get our request parameters
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


// demo Route (GET http://localhost:8080)
mongoose.connect(config.database);
const connection = mongoose.connection
validateData = function (object, validationArray) {
  var validated = true
  var validationErrors = {};
  for (var i = 0; i < validationArray.length; i++) {
    if (!object[validationArray[i]]) {
      if (validated) validated = false
      validationErrors[validationArray[i]] = "This filed is required"
    }
  }
  return { validated: validated, errors: validationErrors }
}
// app.get('/createIndex',function(req,res){
//   connection.db.collection('users').createIndex({ "$**": "text" } ,function(err,good){
//    if(err) return res.status(500).send({ success: false, msg: err});
//     else if(good) return res.status(200).send({ success: true, msg: good });
//   })
// })
app.get('/customers', function (req, res) {
  var perPage = 10
  var page = parseInt(req.query.page) || 1
  var search = req.query.search || ''
  var findObject={};
  if(search)
  findObject={ $text: { $search: search } }
  connection.db.collection('users').find(findObject).toArray(function (error, totalCount) {
      if (totalCount) {
        if (((page * perPage) - perPage) < totalCount.length) {
          let count = totalCount.length
          connection.db.collection('users')
            .find(findObject)
            .skip((perPage * page) - perPage)
            .limit(perPage)
            .toArray(function (err, results) {
              if (err) {
                if (err = [])
                  return res.status(200).send({ success: true, msg: [] });
                else res.status(500).send({ success: false, msg: "An server error has ocured" });
              }
              else {
                if (totalCount) {
                  var returnData = { current: page, data: results, total: count }
                  if (page == 1)
                    returnData['previous'] = null
                  else if (page > 1)
                    returnData['previous'] = 'http://localhost:' + port + '/customers?page=' + (page - 1)
                    +(search?'&search='+search:'')
                  if (count > (page * perPage))
                    returnData['next'] = 'http://localhost:' + port + '/customers?page=' + (page + 1)
                    +(search?'&search='+search:'')
                  else returnData['next'] = null
                  return res.status(200).send({ success: true, data: returnData });
                }
              }
            })
        }
        else res.status(420).send({ success: false, msg: "The page is not found" });
      }

    })

});
app.get('/customer/:id', function (req, res) {
  if (req.params.id) {
    const details = { '_id': new ObjectID(req.params.id) };
    connection.db.collection('users').findOne(details, (err, item) => {
      if (err) res.status(40).send({ success: false, errors: err.errmsg });
      else res.status(200).send({ success: true, data: item });
    })
  }
  else res.status(404).send({ success: false, errors: "You didn't provide id" });
})
app.post('/customer', function (req, res) {
  var validationFields = ["name", "surname", "email", "city", "state", "address"]
  var validatedObject = {};
  validatedObject = validateData(req.body, validationFields)
  if (validatedObject.validated) {
    var collectionExist = false
    connection.db.listCollections('users').next(function (err, collinfo) {
      if (collinfo)
        collectionExist = true;
    })
    if (!collectionExist) {
      connection.db.createCollection('users', function (err, res) {
        if (err) throw err;

      })
    }
    if (!req.body.orders)
      req.body.orders = []
    connection.db.collection('users').insert(req.body, function (err, data) {
      if (err)
        return res.status(500).send({ success: false, msg: err.errmsg });
      else res.status(200).send({ success: true, data: data.ops });
    })
  }
  else
    res.status(422).send({ success: false, errors: validatedObject.errors });


});
app.post('/order', function (req, res) {
  var validationFields = ["name", "amount", "customerId"]
  validatedObject = validateData(req.body, validationFields)
  if (validatedObject.validated) {
    const details = { '_id': new ObjectID(req.body.customerId) };
    delete req.body.customerId
    connection.db.collection('users').findOne(details, (err, item) => {
      if (err)
        res.status(404).send({ success: false, error: err.errmsg });
      else {
        item.orders.push(req.body)
        connection.db.collection('users').update(details, item, function (err, user) {
          if (err) {
            res.status(404).send({ success: false, error: err.errmsg });
          } else {
            res.status(200).send({ success: true, data: item })
          }
        })
      }
    })
  }
  else res.status(404).send({ success: false, errors: validatedObject.errors });
})
app.put('/customer', function (req, res) {
  var validationFields = ["name", "surname", "email", "city", "state", "address"]
  var validatedObject = {};
  validatedObject = validateData(req.body, validationFields)
  if (validatedObject.validated) {
    const details = { '_id': new ObjectID(req.body._id) };
    delete req.body._id
    if (!req.body.orders)
      req.body.orders = []
    connection.db.collection('users').update(details, req.body, function (err, user) {
      if (err) {
        res.status(404).send({ success: false, error: err.errmsg });
      } else {
        res.status(200).send({ success: true, data: user })
      }
    })
  }
  else res.status(422).send({ success: false, errors: validatedObject.errors });
})
app.delete('/customer/:id', function (req, res) {
  if (req.params.id) {
    const details = { '_id': new ObjectID(req.params.id) };
    connection.db.collection('users').remove(details, (err, item) => {
      if (err) {
        res.status(404).send({ success: false, error: err.errmsg });
      } else {
        res.status(200).send({ success: true, data: item })
      }
    })
  }
  else res.status(404).send({ success: false, errors: "You didn'tttt provide id" });
})
app.get('/', function (req, res) {
  res.send('Hello! The API is at http://localhost:' + port + '/api');
})

// Start the server
app.listen(port);
console.log('We are live http://localhost:' + port);